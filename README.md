Habitab

Guys, we will have our whole code base here.

While working on the project, please work on a different branch and not on "Master branch"

clone the project and create a different branch this will even help us to play around with the code and incase we mess up, the code on the master branch will be as it is.

Only the proper code(production code : p) will be present in the master branch, so that we dont mess up with the code if do any improvements.

Use the issue board for any issues faced or noticed while working on the project.

Before pushing the code to master it is better to get the code reviewed.

Commit message : If you commit any changes to the code please commit it with a proper message, as it will be really helpful in debugging, same follows if we merging someone's code with master branch.


.............................. Do not work on the master branch ............................
To clone the repo:
git clone <repo-url>

To create a new branch on the local machine :
git checkout -b <branch-name> 

To push code the repo:
git add . (. will add all the files, for specifying individual files instead dot you will be specifying the file name)
git commit -m "Some message about the code whihc is been pushing"
git push origin <branch-name> - will push the code


To get the code from some branch:
git pull <branch-name>

To merge files you need to look for the mergetool : meld ( Not sure with the name)


For more info:
https://dzone.com/articles/top-20-git-commands-with-examples



-------------------------------------------------------------------------------------
To run the file

To install virtual env

pip3 install virtualenv

1. Create virtual environment

python3 -m venv some_name
Ex: python3 -m venv env

(Or)

virtualenv some_name
Ex : virtualenv env


2. Activate virtual environment

source some_name/bin/activate
Ex : source venv/bin/activate

3. Install all the requirements

 pip3 install -r requirements.txt

4. Run app.py

	python3 app.py




