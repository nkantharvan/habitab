from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
import requests
import dbdata as db
from datetime import timedelta
import flask_login
from flask_cors import CORS
import os


app = Flask(__name__)
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=30)
CORS(app, resources={r"/*": {"origins": "*"}})

total_number_of_request = 0
def request_counter(function_name):
	global total_number_of_request
	total_number_of_request += 1
	def function(*args):
		return function_name(*args)
	return function

# REST apis for the serviceman view
@request_counter
@app.route('/api/v1/serviceman_signup', methods = ["POST", "GET", "DELETE", "PUT"])
def serviceman_signup():
	if request.method == "POST":
		serviceman_object = request.get_json()

		location_coordinates = db.get_geocoding(serviceman_object["address"] + serviceman_object["place"])		
		serviceman_object["latitude"] = location_coordinates[0]
		serviceman_object["longitude"] = location_coordinates[1]

		parameters = list()
		parameters.extend(["emailId"])
		print(db.check_name_in_db(serviceman_object, parameters))
		if db.check_name_in_db(serviceman_object, parameters) == False:
			db.add_user_to_db(serviceman_object)
			return jsonify({}), 201
		else:
			return jsonify({}), 400
	else:
		return jsonify({}), 405
	

@request_counter
@app.route('/api/v1/serviceman_login', methods = ["POST", "GET", "DELETE", "PUT"])
def serviceman_login():
	if request.method == "POST":
		serviceman_object = request.get_json()
		parameters = list()
		parameters.extend(["emailId", "password"])
		
		if db.check_name_in_db(serviceman_object, parameters):
			session["logged_in"] = True
			session["name"] = serviceman_object["emailId"]
			session.permanent = True
			print(session.get("name"))
			return jsonify({}), 200
		else:
			return jsonify({}), 404
	
	else:
		return 405


@request_counter
@app.route('/api/v1/get-serviceman-issue-board/<name>', methods = ["POST", "GET", "DELETE", "PUT"])
def get_issue_board(name):
		if request.method == "GET":
				issue_board = db.get_serviceman_issue_board(name)
				return jsonify(issue_board), 200
		else:
			return jsonify(), 405


@request_counter
@app.route('/api/v1/mark-serviceman-unavailable', methods = ["POST", "GET", "DELETE", "PUT"])
def mark_serviceman_unavailable():
		if request.method == "POST":
				data = request.get_json()
				db.mark_serviceman_unavailable(session.get("name"), data["startDate"], data["endDate"])
				return jsonify({}), 201
		else:
			return jsonify(), 405


@request_counter
@app.route('/api/v1/mark-issue-completed', methods = ["POST", "GET", "DELETE", "PUT"])
def mark_issue_complete():
	if request.method == "POST":
		issue_data = request.get_json()
		issue_feedback = db.change_issue_status(issue_data)
		return jsonify({"feedback" : issue_feedback}), 200
	else:
		return jsonify({}), 405


@request_counter
@app.route("/api/v1/get-serviceman-name-issue-assignment/<latitude>/<longitude>/<location>/<serviceman_type>", methods = ["GET", "POST", "DELETE", "PUT"])
def get_serviceman_name_issue_assignment(latitude, longitude, location, serviceman_type):
	if request.method == "GET":
		tenant_latitude = latitude
		tenant_longitude = longitude
		service_man = db.get_service_man_name(tenant_latitude, tenant_longitude, serviceman_type)
		if service_man != False:
			return jsonify(service_man), 200
		else:
			return jsonify({}), 400
	else:
		return jsonify({}), 405



@app.route("/api/v1/get-total-number-of-request", methods = ["GET", "POST", "DELETE", "PUT"])
def get_total_number_of_request():
	if request.method == "GET":
		global total_number_of_request
		return jsonify({"totalNumberOfServiceManViewRequest" : total_number_of_request}), 200
	else:
		return jsonify({}), 405


# Function for rendering the frontend templates.
@request_counter
@app.route("/homepage")
def homepage():
	return render_template("Index.html")


@request_counter
@app.route("/signup_page")
def signup_page():
	return render_template("Signup.html")


@request_counter
@app.route("/login_page")
def login_page():
	if not session.get('logged_in'):
		return render_template("Login.html")
	else:
		return render_template("Index.html")


@request_counter
@app.route("/user_logout")
def user_logout():
	session["logged_in"] = False
	return render_template("Login.html")


@request_counter
@app.route("/serviceman_issueboard")
def serviceman_issueboard():
	if not session.get("logged_in"):
		return render_template("Login.html")
	else:
		response_data = requests.get("http://127.0.0.1:5001/api/v1/get-serviceman-issue-board/" + session.get("name"))
		return render_template("IssueBoard.html", result = response_data.json())


@request_counter
@app.route("/mark-unavailaibility")
def mark_unavailaibility():
	if not session.get("logged_in"):
		return render_template("Login.html")
	else:		
		return render_template("Mark-Unavailaibility.html")	


if __name__ == "__main__":
	app.secret_key = os.urandom(12)
	app.run(debug=True, port = 5001)