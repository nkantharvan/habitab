import sqlite3
from haversine import haversine, Unit
import requests
import justdialapi
# import hashlib
# hashlib.sha1((user['password']).encode()).hexdigest()


# Function for adding the user name to database
# Parameters : user_dictionary
def add_user_to_db(serviceman_data):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	cursor.execute('''INSERT INTO ServiceMan (userName, password, phoneNumber, address, category, place, state, 
		emailId, latitude, longitude, rating, numberOfPeopleRated) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
		''',[serviceman_data["userName"], serviceman_data["password"], serviceman_data["phoneNumber"], serviceman_data["address"], 
		serviceman_data["category"], serviceman_data["place"], serviceman_data["state"], serviceman_data["emailId"], serviceman_data["latitude"],
		serviceman_data["longitude"], 0, 0])
	connection_state.commit()
	connection_state.close()
	

# Function for checking if the user name exist in the db or no:
# Parameter : 
#	user: user_data
# 	key : a list of parameters which can be used for checking if the user exist in the database.
# 	table_name : Table name in which the particular user should be checked . (Ex : User, ServiceMan)
# returns True : If user exist in the db
# returns False : If the user does not exist in the db.
def check_name_in_db(user_data, key):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	
	query_parameter = [user_data[parameter] for parameter in key]
	parameter_length = len(key)
	query_clause = str()
	
	for i in range(parameter_length):
		if i != parameter_length - 1:
			query_clause += key[i] + " = ? and "
		else:
			query_clause += key[i] + " = ? "
	
	query = "SELECT * from ServiceMan  where " + query_clause
	cursor.execute(query, query_parameter)
	user_data = cursor.fetchone()
	connection_state.commit()
	connection_state.close()
	
	if user_data == None:
		return False
	
	if len(user_data) == 0:
		return False
	
	if user_data != None:
		return True
	
	if len(user_data) != 0:
		return True
	

def get_serviceman_issue_board(name):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = '''SELECT Issue.id, User.userName, Issue.issueDate, Issue.status, Issue.description, Issue.serviceManStatus , Issue.customerStatus 
	from Issue INNER JOIN User ON User.emailId == Issue.reporterMailId and Issue.status == 0 and Issue.serviceManMailId ==  ? '''
	issue_data_from_db = cursor.execute(query, [name, ])
	issue_data_from_db = cursor.fetchall()

	issue_list = list()
	for issue in issue_data_from_db:
		issue_dictionary = dict()
		issue_dictionary["id"] = issue[0]
		issue_dictionary["userName"] = issue[1]
		issue_dictionary["issueDate"] = issue[2]
		issue_dictionary["status"] = issue[3]
		issue_dictionary["description"] = issue[4]
		issue_dictionary["contactPersonStatus"] = issue[5]
		issue_dictionary["customerStatus"] = issue[6]
		issue_list.append(issue_dictionary)

	print(issue_list)

	return issue_list


def mark_serviceman_unavailable(name, start_date, end_date):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	query = "INSERT INTO UnavailabiltyStatus (serviceManMailId, startDate, endDate)  VALUES (?, ?, ?)"
	cursor.execute(query, [name, start_date, end_date])
	connection_state.commit()
	connection_state.close()
		

def change_issue_status(issue_data):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = "UPDATE Issue SET serviceManStatus = ? where id = ?"
	cursor.execute(query, [1, issue_data["issueId"]])
	
	query = "SELECT customerStatus FROM ISSUE  where id = ?"
	cursor.execute(query, [issue_data["issueId"]])
	customer_issue_status = cursor.fetchone()
	
	connection_state.commit()
	connection_state.close()

	if customer_issue_status[0] == 0:
		return "Awaiting For Customer Feedback"
	else:
		return "Done"
	
def get_distance(source_coordinates, destination_cordinates):
	# returns the distance in meters
	# Units can be changes to Kilometers,Miles etc.
	return haversine(source_coordinates, destination_cordinates, unit = Unit.METERS)	


def get_service_man_name(tenant_latitude, tenant_longitude, location, serviceman_type):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()	

	query = "SELECT userName, emailId, latitude, longitude FROM ServiceMan where category = ?"
	cursor.execute(query, [serviceman_type])

	service_man_data = cursor.fetchall()

	if service_man_data == None:
		return False
	if len(service_man_data) == 0:
		return False
	
	min_distance = float("Inf")
	issue_assign_service_man = dict()
	
	distance = float()
	for details in service_man_data:
		service_man_latitude = details[2]
		service_man_longitude = details[3]
		distance = get_distance((float(tenant_latitude), float(tenant_longitude)), 
					(float(service_man_latitude), float(service_man_longitude)))
		if distance < min_distance:
			issue_assign_service_man["userName"] = details[0]
			issue_assign_service_man["emailId"] = details[1]
			issue_assign_service_man["latitude"] = details[2]
			issue_assign_service_man["longitude"] = details[3]
	
	return issue_assign_service_man		


def get_geocoding(location):
    access_token = "498f3c51-5179-49a2-8914-c8ebf05fe7bb"   # update this every 24 hours
    URL = "https://atlas.mapmyindia.com/api/places/geocode?"

    address = location
    auth = access_token

    PARAMS = {'address':address}
    HEADERS = {'Authorization':access_token}

    response = requests.get( url=URL, params= PARAMS, headers = HEADERS)

    if response.status_code == 200:
        data = response.json()
        lat = data['copResults']['latitude']
        lon = data['copResults']['longitude']
        return lat,lon

    else:
        print(response.status_code)