from bs4 import BeautifulSoup
import requests
import csv

def extract_source(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'}
    source=requests.get(url, headers=headers)
    return source

def get_name(body):
	return body.find('span', {'class':'jcn'}).a.string

def get_phone_number(body):
	try:
		return body.find('p', {'class':'contact-info'}).span.a.string
	except AttributeError:
		return ''

def get_rating(body):
	rating = 0.0
	text = body.find('span', {'class':'star_m'})
	if text is not None:
		for item in text:
			rating += float(item['class'][0][1:])/10

	return rating



def get_serviceman_name(serviceman_type, location):
	page_number = 1

	url = "https://www.justdial.com/Bangalore/" + serviceman_type + "-in-" + location
	page = extract_source(url)

	index = url.rindex("/")

	soup = BeautifulSoup(page.content, "html.parser")
	services = soup.find_all('li', {'class': 'cntanr'})
	# print(services)
	names = [get_name(service) for service in services]
	phnos = [get_phone_number(service) for service in services] #not working
	ratings = [get_rating(service) for service in services]
	links = ['https://www.justdial.com/Bangalore/search?q='+name for name in names]

	fields = ['Name', 'Phone', 'Rating', 'link']

	service_man = []
	for i in range(len(services)):
	    dict = {}
	    dict['Name'] = names[i]
	    dict['Phone'] = phnos[i]
	    dict['Rating'] = ratings[i]
	    dict['link'] = links[i]
	    service_man.append(dict)


	service_man = sorted(service_man, key = lambda x : x["Rating"], reverse = True)
	return service_man[0]
