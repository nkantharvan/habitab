var serverAddress = '127.0.0.1'
var portNumber = '5001'

function raiseError(errorMessage) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#FFFFFF")
  $("#outputFeedBack").html(errorMessage)
}


function extractParameters(issueId){
  var issueObject = {}
  issueObject["issueId"] = issueId;
}

function checkForAllParameters(issueObject){
  if(issueObject["issueId"] == '')
      return false;
  return true;
}


function markIssueCompleted(issueId){
  var issueObject = {}
  issueObject["issueId"] = issueId;
  $("#" + issueId + "-Feedback").css("display", "block")
  if(checkForAllParameters(issueObject)){
      $.ajax({
        url : 'http://' + serverAddress + ':' + portNumber + '/api/v1/mark-issue-completed',
        dataType : "json",
        type : "POST",
        contentType: "application/json",
        xhrFields: {withCredentials: false},
        crossDomain: true,
        data: JSON.stringify(issueObject),
        success: function(data) {
            $("#" + issueId).remove();
            
            var newDivElement = document.createElement("div")
            newDivElement.setAttribute("class", "button button-block")
            newDivElement.setAttribute("id", issueId)
            
            var pElement = document.createElement("p")
            pElement.setAttribute("id", issueId + "-value")

            newDivElement.append(pElement)
            $("#" + issueId + "-Status").after(newDivElement)
            $("#" + issueId + "-Feedback").css("display", "none")

            if (data["feedback"] == "Awaiting For ServiceMan Feedback"){
                $("#" + issueId + "-value").html("Awaiting For ServiceMan Feedback")
                $("#" + issueId).css("background","#F6A800")
                $("#" + issueId).css("text-align","center")
            } else {
                $("#" + issueId + "-value").html("Done")
                $("#" + issueId).css("background","#1ba94c")
                $("#" + issueId).css("text-align","center")
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          statusCode = (jqXHR.status);
          raiseError("Please Provide Proper Credentials")
        }
      });
  } else {
      raiseError("Please Provide Proper Credentials")
  }           
}

