var serverAddress = '127.0.0.1'
var portNumber = '5001'

function raiseError(errorMessage) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#FFFFFF")
  $("#outputFeedBack").html(errorMessage)
}

function extractUserParameters(dateObject){
    dateObject["startDate"] = $("#startDate").val()
    dateObject["endDate"] = $("#endDate").val()
    return dateObject;
}

function checkForAllParameters(dateObject){
  if(dateObject["startDate"] == '')
      return false;
  if(dateObject["endDate"] == '')
      return false;
  return true;
}


$('#mark_unavailable').on('click', function(e) {
          
  var dateObject = {}
  console.log("in user signup function")
  dateObject = extractUserParameters(dateObject);
  console.log(dateObject)
  if(checkForAllParameters(dateObject)){
      $.ajax({
        url : 'http://' + serverAddress + ':' + portNumber + '/api/v1/mark-serviceman-unavailable',
        dataType : "json",
        type : "POST",
        contentType: "application/json",
        xhrFields: {withCredentials: false},
        crossDomain: true,
        data: JSON.stringify(dateObject),
        success: function(data) {
            window.location.href = 'http://' + serverAddress + ':' + portNumber + '/login_page'
        },
        error: function(jqXHR, textStatus, errorThrown) {
          statusCode = (jqXHR.status);
          raiseError("Please Provide Proper Credentials")
        }
      });
  } else {
      raiseError("Please Provide Proper Credentials")
  }           
});

