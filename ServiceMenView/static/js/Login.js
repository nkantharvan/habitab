var serverAddress = '127.0.0.1'
var portNumber = '5001'

function raiseError(errorMessage) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#f50000")
  $("#outputFeedBack").css("font-size","30px")
  $("#outputFeedBack").html(errorMessage)
}

function extractUserParameters(userObject){
    userObject["emailId"] = $("#emailId").val()
    userObject["password"] = $("#password").val()
    return userObject;
}

function checkForAllParameters(userObject){
  if(userObject["emailId"] == '')
      return false;
  if(userObject['password'] == '')
      return false;
  return true;
}

$('#user_login').on('click', function(e) {
          
  var userObject = {}
  userObject = extractUserParameters(userObject);
  if(checkForAllParameters(userObject)){
      $.ajax({
        url : 'http://' + serverAddress + ':' + portNumber + '/api/v1/serviceman_login',
        dataType : "json",
        type : "POST",
        contentType: "application/json",
        xhrFields: {withCredentials: false},
        crossDomain: true,
        data: JSON.stringify(userObject),
        success: function(data) {
            window.location.href = 'http://' + serverAddress + ":" + portNumber + '/homepage'
        },
        error: function(jqXHR, textStatus, errorThrown) {
          statusCode = (jqXHR.status);
          raiseError("The given email id is already registered with us")
        }
      });
  } else {
      raiseError("Please Provide Proper Credentials")
  }           
});