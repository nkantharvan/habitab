import sqlite3 as  db
connection = db.connect("Habitab.db")
cursor = connection.cursor()

# cursor.execute('''CREATE TABLE User
# 					(userName TEXT,
# 					 address TEXT,
# 					 place TEXT,
# 					 state TEXT,
# 					 emailId TEXT,
# 					 password TEXT,
# 					 PRIMARY KEY(userName)
# 					)
# 				''')
# connection.commit()


# cursor.execute('''CREATE TABLE ServiceMan
# 					(userName TEXT,
# 					 password TEXT,
# 					 phoneNumber INTEGER,
# 					 address TEXT,
# 					 category TEXT,
# 					 place TEXT,
# 					 state TEXT,
# 					 emailId TEXT,
# 					 latitude REAL, 
# 					 longitude REAL,
# 					 PRIMARY KEY(emailId)
# 					)
# 				''')
# connection.commit()

# cursor.execute('''CREATE TABLE Issue
# 					(
# 					 id INTEGER PRIMARY KEY AUTOINCREMENT,
# 					 reporterMailId TEXT,
# 					 issueDate DATE,
# 					 description TEXT,
# 					 category TEXT,
# 					 serviceManMailId TEXT,
# 					 status TEXT,
# 					 customerStatus	TEXT,
# 					 contactPersonStatus TEXT,
# 					 FOREIGN KEY(reporterMailId) references User(emailId), 
# 					 FOREIGN KEY(serviceManMailId) references ServiceMan(emailId)
# 					)
# 				''')
# connection.commit()

# connection.close()


cursor.execute('''CREATE TABLE UnavailabiltyStatus
					(
					 id INTEGER PRIMARY KEY AUTOINCREMENT,
					 serviceManMailId TEXT,
					 startDate DATE,
					 endDate DATE, 
					 FOREIGN KEY(serviceManMailId) references ServiceMan(emailId)
					)
				''')
connection.commit()
connection.close()