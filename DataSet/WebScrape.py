from bs4 import BeautifulSoup
import requests
import csv
from lxml import html
import re

def split_html_tag(html_element):
    try:
        text_value = html_element.split("</span>")
        text_value = text_value[0].split('>')[1]
        return text_value
    except:
        return ''

with open("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/DataSet/Rent_Area_Wise.csv", "w") as file_pointer:
    writer = csv.writer(file_pointer)
    writer.writerow(["Area", "1BHK", "Average Price 1 BHK", "2BHK", "Average Price 2 BHK", "3BHK", "Average Price 3 BHK"])

    for page_number in range(1, 81):
        url = "https://www.makaan.com/price-trends/property-rates-for-rent-in-bangalore?page=" + str(page_number)

        html_content = requests.get(url).text

        soup = BeautifulSoup(html_content, "lxml")


        rent_table = soup.find("table", attrs={"class": "tbl"})
        rent_table_data = rent_table.tbody.find_all("tr") 
        rent_table_data[1:len(rent_table_data)]
        
        for rows in rent_table_data:
            place_name = rows.find('span',{'itemprop':'name'})
            place_name = split_html_tag(str(place_name))
            
            pattern = re.compile(r'<span itemprop="minPrice">\d+,\d+</span>')
            min_price = pattern.findall(str(rows))
            min_price_list = []
            for i in min_price:
                min_price_list.append(split_html_tag(str(i)))

            pattern = re.compile(r'<span class="currency"></span> \d+,\d+')
            average_price_list = [i.split("</span> ")[1] for i in pattern.findall(str(rows)) ]


            try:
                writer.writerow([place_name, min_price_list[0], average_price_list[0], min_price_list[1], average_price_list[1],
                 min_price_list[2], average_price_list[2]])
            except:
                try:
                    writer.writerow([place_name, min_price_list[0], average_price_list[0], min_price_list[1], average_price_list[1], "NULL", "NULL"])
                except:
                    try:
                        writer.writerow([place_name, min_price_list[0], average_price_list[0], "NULL", "NULL", "NULL", "NULL"])
                    except:
                        continue

            

            
            

