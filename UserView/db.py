import sqlite3 as  db
connection = db.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
cursor = connection.cursor()

# cursor.execute('''CREATE TABLE User
# 					(userName TEXT,
# 					 address TEXT,
# 					 place TEXT,
# 					 latitude REAL,
# 					 longitude REAL,
# 					 state TEXT,
# 					 emailId TEXT,
# 					 password TEXT,
# 					 companyName TEXT,
# 					 companyLocation TEXT,
# 					 rating INTEGER,
# 					 numberOfPeopleRated INTEGER,
# 					 PRIMARY KEY(userName)
# 					)
# 				''')
# connection.commit()

# cursor.execute('''CREATE TABLE Property
# 					(pid INTEGER PRIMARY KEY AUTOINCREMENT,
# 					 image TEXT,
# 					 locality TEXT,
# 					 latitude REAL,
# 					 longitude REAL,
# 					 noOfBedRooms INTEGER,
# 					 rentAmount REAL,
# 					 description TEXT,
# 					 landlordEmailId TEXT,
# 					 rating INTEGER,
# 					 numberOfPeopleRated INTEGER,
# 					 FOREIGN KEY(landlordEmailId) REFERENCES Landlord(emailId)
# 					)
# 				''')
# connection.commit()




# cursor.execute('''CREATE TABLE PropertyAssignment
# 					(pid INTEGER PRIMARY KEY,
# 					 tenantEmailId TEXT,
# 					 rentRemainderDate DATE,
# 					 FOREIGN KEY(pid) REFERENCES Property(pid),
# 					 FOREIGN KEY(tenantEmailId) REFERENCES User(emailId)
# 					)
# 				''')
# connection.commit()


# cursor.execute('''CREATE TABLE PropertyAvalaibilityStatus
# 					(pid INTEGER PRIMARY KEY,
# 					 tenantApproval INTEGER,
# 					 landlordApproval INTEGER,
# 					 FOREIGN KEY(tenantEmailId) REFERENCES User(emailId),
# 					 FOREIGN KEY(pid) REFERENCES Property(pid)
# 					)
# 				''')
# connection.commit()

# cursor.execute('''CREATE TABLE TenantInterest
# 					(pid INTEGER PRIMARY KEY,
# 					 tenantEmailId TEXT,				
# 					 FOREIGN KEY(tenantEmailId) REFERENCES User(emailId),
# 					 FOREIGN KEY(pid) REFERENCES Property(pid)
# 					)
# 				''')
# connection.commit()

cursor.execute('''CREATE TABLE TEST
					(pid INTEGER PRIMARY KEY AUTOINCREMENT
					)
				''')
connection.commit()


# cursor.execute('''CREATE TABLE Payment
# 					(pid INTEGER,
# 					 tenantEmailId TEXT,
# 					 paymentDate DATE,
# 					 paymentMode TEXT,
# 					 amount TEXT,
# 					 PRIMARY KEY(pid, tenantEmailId, paymentDate),
# 					 FOREIGN KEY(pid) REFERENCES Property(pid),
# 					 FOREIGN KEY(tenantEmailId) REFERENCES User(emailId)
# 					)
# 				''')
# connection.commit()


# cursor.execute('''CREATE TABLE Issue
# 					(
# 					 id INTEGER PRIMARY KEY AUTOINCREMENT,
# 					 reporterMailId TEXT,
# 					 issueDate date,
# 					 description TEXT,
# 					 category TEXT,
# 					 contactPersonMailId TEXT,
# 					 status TEXT,
# 					 customerStatus	TEXT,
# 					 contactPersonStatus TEXT,
# 					 FOREIGN KEY(reporterMailId) references User(emailId), 
# 					 FOREIGN KEY(contactPersonMailId) references ContactPerson(emailId)
# 					)
# 				''')
# connection.commit()


# connection.close()