import sqlite3
from datetime import date
from haversine import haversine, Unit
import requests
# import hashlib
# hashlib.sha1((user['password']).encode()).hexdigest()


# Function for adding the user name to database
# Parameters : user_dictionary


def add_user_to_db(user_data):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	cursor.execute('''INSERT INTO User (userName, address, place, latitude, longitude, state, emailId, password, 
		companyName, companyLocation, rating, numberOfPeopleRated) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
			[user_data["userName"], 
			user_data["address"], 
			user_data["place"], 
			user_data["latitude"], 
			user_data["longitude"],
			user_data["state"], 
			user_data["emailId"], 
			user_data["password"], 
			user_data["companyName"], 
			user_data["companyLocation"] , 
			0, 
			0])
	connection_state.commit()
	connection_state.close()
	

# Function for checking if the user name exist in the db or no:
# Parameter : 
#	user: user_data
# 	key : a list of parameters which can be used for checking if the user exist in the database.
# returns True : If user exist in the db
# returns False : If the user does not exist in the db.
def check_user_name_in_db(user_data, key):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	
	query_parameter = [user_data[parameter] for parameter in key]
	
	parameter_length = len(key)
	query_clause = str()
	
	for i in range(parameter_length):
		if i != parameter_length - 1:
			query_clause += key[i] + " = ? and "
		else:
			query_clause += key[i] + " = ? "
	
	query = "SELECT * from User where " + query_clause
	cursor.execute(query, query_parameter)
	user_data = cursor.fetchall()
	connection_state.commit()
	connection_state.close()
	
	if len(user_data) == 0:
		return False
	elif user_data == None:
		return False
	else:
		return True


def add_issue_to_db(issue_data):

	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	
	cursor.execute('''INSERT INTO Issue (reporterMailId, issueDate, description, category, serviceManMailId, status, customerStatus, 
		serviceManStatus) VALUES (?, ?, ?, ?, ?, ?, ?, ?)
		''',[issue_data["reporterMailId"], 
			 issue_data["issueDate"], 
			 issue_data["description"],  
			 issue_data["issueCategory"], 
			issue_data["serviceManMailId"],
			issue_data["status"], 
			0, 
			0])
	
	connection_state.commit()
	connection_state.close()


def get_contact_person_details(attribute_list, filter_attribute, filter_attribute_value):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	
	query_parameter = str()
	filter_attribute_length = len(filter_attribute)
	
	for i in range(filter_attribute_length):
		if i != filter_attribute_length - 1:
			query_parameter += filter_attribute[i] + " = ? and "
		else:
			query_parameter += filter_attribute[i] + " = ? "
	
	data = tuple()
	
	attribute_string = "( " + " ".join(attribute_list) + " )"
	
	if filter_attribute_length != 0:
		query = "SELECT " + attribute_string + " from ServiceMan where " + query_parameter 
		cursor.execute(query, filter_attribute_value)
		data = cursor.fetchall()
	
	else:
	 	query = "SELECT " + attribute_string + " from ServiceMan "
	 	cursor.execute(query, filter_attribute_value)
	 	data = cursor.fetchall()
	 
	connection_state.commit()
	connection_state.close()

	contact_person = list()
	attribute_list_length = len(attribute_list)
	for details in data:
		person_details = dict()
		for i in range(attribute_list_length):
			person_details[attribute_list[i]] = details[i]
		contact_person.append(person_details)

	return contact_person 

def get_issue_details_from_db(emailId):

	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	cursor.execute('''SELECT * from Issue where reporterMailId = ?''', (emailId,))

	issue_data = cursor.fetchall()
	issue_board = dict()
	# issue Format : ("issueId", "reportedMailId", "issueDate", "description", "category", "serviceManMailId", "status", "customerStatus","serviceManStatus")
	for issue in issue_data:
		issue_board[issue[0]] = dict()
		issue_board[issue[0]].update({"issueDate" : issue[2]})
		issue_board[issue[0]].update({"issueDescription" : issue[3]})
		issue_board[issue[0]].update({"issueCategory" : issue[4]})
		issue_board[issue[0]].update({"issueStatus" : issue[6]})
		issue_board[issue[0]].update({"customerStatus" : issue[7]})
		issue_board[issue[0]].update({"serviceManStatus" : issue[8]})
		
		service_man_mail_id = issue[5]
		attribute_list = list()
		attribute_list.append("userName")

		filter_attribute = list()
		filter_attribute.append("emailId")

		filter_attribute_value =list()
		filter_attribute_value.append(service_man_mail_id)
		contact_person = get_contact_person_details(attribute_list, filter_attribute, filter_attribute_value)

		issue_board[issue[0]].update({"serviceManName" : contact_person[0]['userName']})
		issue_board[issue[0]].update({"serviceManMailId" : service_man_mail_id})
	return issue_board

def change_issue_status(issue_data):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = "UPDATE Issue SET customerStatus = ? , customerFeedback = ? where id = ? "
	cursor.execute(query, [1, issue_data["customerFeedback"], issue_data["issueId"]])
	connection_state.commit()

	query = "SELECT serviceManStatus FROM ISSUE  where id = ?"
	cursor.execute(query, [issue_data["issueId"]])
	customer_issue_status = cursor.fetchone()
	
	query = '''UPDATE ServiceMan SET rating = (rating + ?)/(numberOfPeopleRated + 1) , numberOfPeopleRated = (numberOfPeopleRated + 1) 
	where emailId = ? '''
	cursor.execute(query, [issue_data["customerRating"], issue_data["serviceManMailId"].rstrip().lstrip()])
	
	connection_state.commit()
	connection_state.close()

	if customer_issue_status[0] == 0:
		return "Awaiting For ServiceMan Feedback"
	else:
		return "Done"

def get_distance(user_location_coordinates, house_cordinates):
	# returns the distance in meters
	# Units can be changes to Kilometers,Miles etc.
	return haversine(user_location_coordinates, house_cordinates, unit = Unit.METERS)

	
def filter_house(house_data, latitude = None, longitude = None):
	# Distance_measure is a dictionary which will have distance between user location and the house as a key and the value to that will be house details.
	distance_measure = dict()

	for detail in house_data:
		distance = get_distance((float(latitude), float(longitude)), (float(detail[4]), float(detail[5])))
		# Distance is being casted to the str, because the distance being returned is a float and the keys cannot be float.
		if str(distance) not in distance_measure:
			distance_measure[str(distance)] = []
			distance_measure[str(distance)].append(detail)
		else:
			distance_measure[str(distance)].append(detail)
	
	distance_measure = sorted(distance_measure.items(), key = lambda x : float(x[0]))
	# This is again being converted to dict because after the sorting the format changes to : [key, [Values]] so casting it back to dict gives us the
	# dictionary format.
	distance_measure = dict(distance_measure)

	house_board = []
	number_of_house = 0
	for key, value in distance_measure.items():
		for detail in value:
			house_details = dict()
			house_details["houseId"] = detail[0]
			house_details["description"] = detail[1]
			house_details["image"] = detail[2]
			house_details["location"] = detail[3]
			house_details["latitude"] = detail[4]
			house_details["longitude"] = detail[5]
			house_details["noOfBedrooms"] = detail[6]
			house_details["rentAmount"] = detail[7]
			house_details["landlordMailId"] = detail[8]
			house_details["rating"] = detail[9]
			house_board.append(house_details)
			number_of_house += 1
		
		# Sending Top 10 homes
		if number_of_house == 10:
			break

	return house_board


def get_house_data(latitude = None, longitude = None, no_of_bedrooms = None):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	if no_of_bedrooms == None:
		query = ''' SELECT Property.pid, Property.description, Property.image, Property.locality, Property.latitude, Property.longitude, Property.landlordEmailId,
					Property.noOfBedrooms, Property.landlordEmailId, Property.rating FROM Property INNER JOIN PropertyAvalaibilityStatus ON PropertyAvalaibilityStatus.pid == Property.pid and 
					PropertyAvalaibilityStatus.tenantApproval == 0 and PropertyAvalaibilityStatus.landlordApproval == 0'''
		cursor.execute(query)
	
	else:
		query = ''' SELECT Property.pid, Property.description, Property.image, Property.locality, Property.latitude, Property.longitude, Property.landlordEmailId,
					Property.noOfBedrooms, Property.landlordEmailId, Property.rating FROM Property INNER JOIN PropertyAvalaibilityStatus ON PropertyAvalaibilityStatus.pid == Property.pid and 
					PropertyAvalaibilityStatus.tenantApproval == 0 and PropertyAvalaibilityStatus.landlordApproval == 0 and Property.noOfBedrooms >= ?'''
		cursor.execute(query, [no_of_bedrooms])
	
	house_data = cursor.fetchall()
	house_board = filter_house(house_data, latitude = latitude, longitude = longitude)
	return house_board
	

	
def get_user_house_details(user_email_id):
	
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = '''SELECT Property.pid, Property.noOfBedrooms, Property.locality, Property.latitude, Property.longitude, Property.description,
			Property.image, Property.landlordEmailId, Property.rentAmount, PropertyAssignment.rentReminderDate from
			PropertyAssignment INNER JOIN Property ON PropertyAssignment.tenantEmailId = ? and PropertyAssignment.pid = Property.pid 
			'''
	
	cursor.execute(query, [user_email_id])
	house_data = cursor.fetchone()

	if house_data == None:
		return False
	
	if len(house_data) == 0:
		return False
	
	user_house_details = dict()

	user_house_details["pid"] = house_data[0]
	user_house_details["noOfBedrooms"] = house_data[1]
	user_house_details["locality"] = house_data[2]
	user_house_details["latitude"] = house_data[3]
	user_house_details["longitude"] = house_data[4]
	user_house_details["description"] = house_data[5]
	user_house_details["image"] = house_data[6]
	user_house_details["landlordEmailId"] = house_data[7]
	user_house_details["rentAmount"] = house_data[8]
	user_house_details["rentReminderDate"] = house_data[9]

	return user_house_details

def add_payment_data_to_db(payment_data):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = "INSERT INTO Payment (pid, tenantEmailId, paymentDate, paymentMode, amount) VALUES (?, ?, ?, ?, ?)"
	cursor.execute(query, [payment_data["pid"], payment_data["tenantEmailId"], 
		payment_data["paymentDate"], 
		payment_data["paymentMode"], 
		payment_data["paymentAmount"]])	
	
	connection_state.commit()
	connection_state.close()


def update_property_status(house_id, user_email_id):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = '''INSERT INTO TenantInterest (pid, tenantEmailId) VALUES (?, ?)'''
	cursor.execute(query, [house_id, user_email_id])
	connection_state.commit()	
	
	query = "UPDATE PropertyAvalaibilityStatus SET tenantApproval = 1 where pid = ?"
	cursor.execute(query, [house_id])
	connection_state.commit()	
	
	return True

def push_data(user_email_id):

	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	today_date = date.today()
	today_date = today_date.strftime("%d-%m-%y")

	query = "SELECT rentReminderDate FROM PropertyAssignment where tenantEmailId = ?"
	cursor.execute(query, [user_email_id])
	reminder_date = cursor.fetchone()
	if reminder_date != None:
		if reminder_date[0] == today_date:
			yield 'data: %s\n\n' % 'Your Rent Payment is due today'.format(user_email_id)


def get_geocoding(location):
    access_token = "498f3c51-5179-49a2-8914-c8ebf05fe7bb"   # update this every 24 hours
    URL = "https://atlas.mapmyindia.com/api/places/geocode?"

    address = location
    auth = access_token

    PARAMS = {'address':address}
    HEADERS = {'Authorization':access_token}

    response = requests.get( url=URL, params= PARAMS, headers = HEADERS)

    if response.status_code == 200:
        data = response.json()
        lat = data['copResults']['latitude']
        lon = data['copResults']['longitude']
        return lat,lon

    else:
        print(response.status_code)