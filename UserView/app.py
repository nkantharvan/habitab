from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify, Response
import requests
import dbdata as db
from datetime import date
from datetime import timedelta
import flask_login
import os

app = Flask(__name__)
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=30)
login_manager = flask_login.LoginManager()

login_manager.init_app(app)


total_number_of_request = 0
def request_counter(function_name):
	global total_number_of_request
	total_number_of_request += 1
	def function(*args):
		return function_name(*args)
	return function

# REST apis for the serviceman view
@request_counter
@app.route('/api/v1/user_signup', methods = ["POST", "GET", "DELETE", "PUT"])
def user_signup():
	if request.method == "POST":
		user_object = request.get_json()

		location_coordinates = db.get_geocoding(user_object["address"] + user_object["place"])		
		user_object["latitude"] = location_coordinates[0]
		user_object["longitude"] = location_coordinates[1]

		parameters = list()
		parameters.extend(["emailId"])
		print(db.check_user_name_in_db(user_object, parameters))

		if db.check_user_name_in_db(user_object, parameters) == False:
			db.add_user_to_db(user_object)
			return jsonify({}), 201
		else:
			return jsonify({}), 400
	else:
		return jsonify({}), 405
	

@request_counter
@app.route('/api/v1/user_login', methods = ["POST", "GET", "DELETE", "PUT"])
def user_login():
	if request.method == "POST":
		user_object = request.get_json()
		parameters = list()
		parameters.extend(["emailId", "password"])
		
		if db.check_user_name_in_db(user_object, parameters):
			session["logged_in"] = True
			session["emailId"] = user_object["emailId"]
			session.permanent = True
			return jsonify({}), 200
		else:
			return jsonify({}), 404
	
	else:
		return 405


@request_counter
@app.route("/api/v1/post_issue", methods = ["POST", "GET", "DELETE", "PUT"])
def post_issue():
	if request.method == "POST":
		
		issue_data = request.get_json()
		print(issue_data)
		today_date = date.today()
		today_date = today_date.strftime("%d-%m-%y")
		
		house_details = db.get_user_house_details(session.get("emailId"))
		# house_details = db.get_user_house_details("msoni6226@gmail.com")
		# print(house_details)
		
		if house_details != False:
			user_latitude = house_details["latitude"]
			user_longitude = house_details["longitude"]
			user_location = house_details["location"]
			
			service_man_data = requests.get("http://127.0.0.1:5001/api/v1/get-serviceman-name-issue-assignment/" + str(user_latitude) 
									+ "/" + str(user_longitude) + "/" + issue_data["location"] + "/" + issue_data["issueCategory"])
			
			print(service_man_data)
			service_man_data = service_man_data.json()

			# issue_data["reporterMailId"] = "msoni6226@gmail.com"
			issue_data["reporterMailId"] = session.get("emailId")
			issue_data["issueDate"] = today_date
			
			# issue_data["contactPersonMailId"] = "msoni6226@gmail.com"
			issue_data["serviceManMailId"] = service_man_data["emailId"]
			issue_data["status"] = 0
			
			parameters = list()
			parameters.append("emailId")
			
			user_data = dict()
			user_data["emailId"] = session.get("emailId")
			
			if db.check_user_name_in_db(user_data, parameters):	
				db.add_issue_to_db(issue_data)
				return jsonify({}), 200
			else:
				return jsonify({}), 400
		else:
			return jsonify({}), 404
	else:
		return jsonify({}), 405


@request_counter
@app.route('/api/v1/mark-issue-completed', methods = ["POST", "GET", "DELETE", "PUT"])
def mark_issue_complete():
	if request.method == "POST":
		issue_data = request.get_json()
		issue_feedback = db.change_issue_status(issue_data)
		return jsonify({"feedback" : issue_feedback}), 200
	else:
		return jsonify({}), 405


@request_counter
@app.route('/api/v1/get-houses-available-for-rent/<location>/<noOfBedrooms>', methods = ["POST", "GET", "DELETE", "PUT"])
def get_houses_available_for_rent(location, noOfBedrooms):
	if request.method == "GET":
		if session.get("logged_in"):
			location_object = {}
			location_object["name"] = location
			location_coordinates = db.get_geocoding(location_object["name"])
			location_object["latitude"] = location_coordinates[0]
			location_object["longitude"] = location_coordinates[1]

			if noOfBedrooms == "all":
				house_data = db.get_house_data(latitude = 12, longitude = 72)
			else:
				house_data = db.get_house_data(latitude = 12, longitude = 72, no_of_bedrooms = int(noOfBedrooms))
			return jsonify(house_data), 200
		else:
			return jsonify({}), 400
	
	else:
		return jsonify({}), 405


@request_counter
@app.route('/api/v1/get-user-house-details', methods = ["POST", "GET", "DELETE", "PUT"])
def get_user_house_details():
	if request.method == "GET":
		# house_details = db.get_user_house_details("msoni6226@gmail.com")
		house_details = db.get_user_house_details(session.get('emailId'))
		return jsonify(house_details), 200
	else:
		return jsonify({}), 405


@request_counter
@app.route("/api/v1/get-user-rent-amount", methods = ["POST", "GET", "DELETE", "PUT"])
def get_user_rent_amount():
	if request.method == "GET":
		# user_email_id = "msoni6226@gmail.com"
		user_email_id = session.get('emailId')
		house_details = db.get_user_house_details(user_email_id)
		return jsonify({"rentAmount": house_details["rentAmount"]}), 200
	else:
		return jsonify({}), 405

@request_counter
@app.route("/api/v1/make-payment", methods = ["POST", "GET", "DELETE", "PUT"])
def make_payment():
	if request.method == "POST":
		user_email_id = session.get('emailId')
		house_details = db.get_user_house_details(user_email_id)
		if house_details != False:
			pid = house_details["pid"]
			today_date = date.today()
			today_date = today_date.strftime("%d-%m-%y")

			payment_data = request.get_json()
			# payment_data["tenantEmailId"] = "msoni6226@gmail.com"
			payment_data["tenantEmailId"] = session.get('emailId')
			payment_data["paymentDate"] = today_date
			payment_data["pid"] = pid

			db.add_payment_data_to_db(payment_data)
			return jsonify({}), 200
		else:
			return jsonify({}), 400
	else:
		return jsonify({}), 405


@app.route("/api/v1/get-total-number-of-request", methods = ["GET", "POST", "DELETE", "PUT"])
def get_total_number_of_request():
	if request.method == "GET":
		global total_number_of_request
		return jsonify({"totalNumberOfUserViewRequest" : total_number_of_request}), 200
	else:
		return jsonify({}), 405


@app.route("/api/v1/book-home", methods = ["POST", "GET", "DELETE", "PUT"])
def book_home():
	if request.method == "POST":
		data = request.get_json()
		house_id = data["houseId"]
		user_email_id = session.get('emailId')
		db.update_property_status(house_id, user_email_id)
		return jsonify({}), 200
	else:
		return jsonify({}), 405


# provide SSE stream to the web browser
@app.route('/rental_reminder')
def stream():
        return Response(db.push_data(session.get('emailId')), mimetype="text/event-stream")
        # return Response(db.push_data(session.get("name")), mimetype="text/event-stream")

@request_counter
@app.route("/search")
def search():
		return render_template("Search.html")

@request_counter
@app.route("/login_page")
def login_page():
	if not session.get('logged_in'):
		return render_template("Login.html")
	else:
		return render_template("Index.html")

@request_counter
@app.route("/signup_page")
def signup_page():
	return render_template("Signup.html")

@request_counter
@app.route("/user_logout")
def user_logout():
	session["logged_in"] = False
	return render_template("Login.html")

@request_counter
@app.route("/report_issue")
def report_issue():
	if session.get("logged_in"):
		return render_template("ReportIssue.html")
	else:
		return render_template("Login.html")

@request_counter
@app.route("/issue_board")
def issue_board():
	if session.get("logged_in"):
		issue_data = db.get_issue_details_from_db(session.get('emailId'))
		print(issue_data)
		return render_template("IssueBoard.html", issue_data = issue_data)
	else:
		return render_template("Login.html")		

@request_counter
@app.route("/payment_portal")
def payment_portal():
	if session.get("logged_in"):
		return render_template("payment.html")
	else:
		return render_template("Login.html")

@request_counter
@app.route("/Reminders")
def Reminders():
	reminder_message = []
	reminder_message.append("Your Rent payment is due today")
	return render_template("Reminders.html", reminder_message = reminder_message)


@request_counter
@app.route("/homepage")
def homepage():
	if session.get("emailId") != None:
		house_details = db.get_user_house_details(session.get('emailId'))
		if(house_details == False):
			return render_template("Index.html", apartmentExists = False)
		else:
			house_data=jsonify(house_details)
			return render_template('Index.html', apartmentExists = True,details = house_details)
	else:
		return render_template("Index.html")

if __name__ == "__main__":
	app.secret_key = os.urandom(12)
	app.run(debug=True, port = 5000)