function raiseError(errorMessage) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#f50000")
  $("#outputFeedBack").css("font-size","30px")
  $("#outputFeedBack").html(errorMessage)
}


function displayMessage(message) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#f50000")
  $("#outputFeedBack").css("font-size","30px")
  $("#outputFeedBack").html(message)
}

function extractIssueParameters(issueObject){
    issueObject["issueCategory"] = $("#issueCategory").val()
    issueObject["description"] = $("#issueDetails").val()
    return issueObject;
}

function checkForAllParameters(issueObject){
  if(issueObject["issueCategory"] == '')
      return false;
  if(issueObject['description'] == '')
      return false;
  return true;
}

$('#submit_issue').on('click', function(e) {
          
  var issueObject = {}
  issueObject = extractIssueParameters(issueObject);
  if(checkForAllParameters(issueObject)){
      console.log(issueObject)    
      $.ajax({
        url : 'http://127.0.0.1:5000/api/v1/post_issue',
        dataType : "json",
        type : "POST",
        contentType: "application/json",
        xhrFields: {withCredentials: false},
        crossDomain: true,
        data: JSON.stringify(issueObject),
        success: function(data) {
            $("#issueCategory").val("")
            $("#issueDetails").val("")
            displayMessage("Issue Submitted");
            window.location.href = 'http://127.0.0.1:5000/issue_board'
        },
        error: function(jqXHR, textStatus, errorThrown) {
          statusCode = (jqXHR.status);
          raiseError("Error in reporting Issue. Plase try after some time")
        }
      });
  } else {
      raiseError("Please Provide Proper Credentials")
  }           
});