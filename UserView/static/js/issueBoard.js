function raiseError(errorMessage) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#FFFFFF")
  $("#outputFeedBack").html(errorMessage)
}


function displayMessage(message) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#FFFFFF")
  $("#outputFeedBack").html(message)
}

function extractIssueParameters(issueObject){
    issueObject["issueCategory"] = $("#issueCategory").val()
    issueObject["description"] = $("#issueDetails").val()
    return issueObject;
}

function checkForAllParameters(issueObject){
  if(issueObject["issueCategory"] == '')
      return false;
  if(issueObject['description'] == '')
      return false;
  return true;
}
