function raiseMessage(message) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#f50000")
  $("#outputFeedBack").css("font-size","30px")
  $("#outputFeedBack").html(message)
}

function extractUserParameters(){
    var paymentObject = {}
    paymentObject["paymentAmount"] = $("#paymentAmount").html().split(":")[1]
    paymentObject["paymentMode"] = $("#paymentMode").val()
    return paymentObject;
}

function checkForAllParameters(paymentObject){
  if(paymentObject["paymentAmount"] == '')
      return false;
  if(paymentObject['paymentMode'] == '')
      return false;
  return true;
}

$("#make_payment").on('click', function(e){      
  var paymentObject = {}
  paymentObject = extractUserParameters(paymentObject);
  if(checkForAllParameters(paymentObject)){
      $.ajax({
        url : 'http://127.0.0.1:5000/api/v1/make-payment',
        dataType : "json",
        type : "POST",
        contentType: "application/json",
        xhrFields: {withCredentials: false},
        crossDomain: true,
        data: JSON.stringify(paymentObject),
        success: function(data) {
            raiseMessage("Payment Done")
        },
        error: function(jqXHR, textStatus, errorThrown) {
          statusCode = (jqXHR.status);
          raiseMessage("You have already made payment for this month")
        }
      });
  } else {
      raiseMessage("Please Provide Proper Credentials")
  }           
});


function getRentAmount(){
    $.ajax({
          url : 'http://127.0.0.1:5000/api/v1/get-user-rent-amount',
          dataType : "json",
          type : "GET",
          contentType: "application/json",
          xhrFields: {withCredentials: false},
          crossDomain: true,
          success: function(data) {
              $("#paymentAmount").html("Payment Amount : " + data["rentAmount"])
          },
          error: function(jqXHR, textStatus, errorThrown) {
            statusCode = (jqXHR.status);
            raiseMessage("Seems like you dont have any Property to your name.")
          }
      });
  paymentAmountObtained = true;
}

setTimeout(getRentAmount, 3000);

