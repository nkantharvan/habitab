var serverAddress = '127.0.0.1'
var portNumber = '5000'

var feedbackBox = false
function raiseError(errorMessage) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#f50000")
  $("#outputFeedBack").css("font-size","30px")
  $("#outputFeedBack").html(errorMessage)
}

function checkForAllParameters(issueObject){
  if(issueObject["issueId"] == '')
      return false;
  if (issueObject["customerFeedback"] == '')
      return false;
  if (issueObject["customerRating"] == '')
      return false;
  return true;
}

function extractParameters(issueId){
  var issueObject = {}
  issueObject["issueId"] = issueId;
  issueObject["customerFeedback"] = $("#" + issueId + "-Feedback-Value").val();
  issueObject["customerRating"] = $("#" + issueId + "-Feedback-Rating").val();
  issueObject["serviceManMailId"] = $("#" + issueId + "-ServiceManMailId").html().split(":")[1]
  return issueObject;
}

function markIssueCompleted(issueId){
  var issueObject = {}
  issueObject = extractParameters(issueId)
  $("#" + issueId + "-Feedback").css("display", "block")
  
  if(checkForAllParameters(issueObject)){
      $.ajax({
        url : 'http://' + serverAddress + ':' + portNumber + '/api/v1/mark-issue-completed',
        dataType : "json",
        type : "POST",
        contentType: "application/json",
        xhrFields: {withCredentials: false},
        crossDomain: true,
        data: JSON.stringify(issueObject),
        success: function(data) {
            $("#" + issueId).remove();
            
            var newDivElement = document.createElement("div")
            newDivElement.setAttribute("class", "button button-block")
            newDivElement.setAttribute("id", issueId)
            
            var pElement = document.createElement("p")
            pElement.setAttribute("id", issueId + "-value")

            newDivElement.append(pElement)
            $("#" + issueId + "-Status").after(newDivElement)
            $("#" + issueId + "-Feedback").css("display", "none")

            if (data["feedback"] == "Awaiting For ServiceMan Feedback"){
                $("#" + issueId + "-value").html("Awaiting For ServiceMan Feedback")
                $("#" + issueId).css("background","#F6A800")
                $("#" + issueId).css("text-align","center")
            } else {
                $("#" + issueId + "-value").html("Done")
                $("#" + issueId).css("background","#1ba94c")
                $("#" + issueId).css("text-align","center")
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          statusCode = (jqXHR.status);
          raiseError("Error in markig the issue complete. Please try after sometime")
        }
      });
  } else {
      raiseError("Please Provide Proper Credentials")
  }           
}

