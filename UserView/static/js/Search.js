function raiseError(errorMessage) {
  console.log(errorMessage);
  $("#searchDisplay").html("");
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#FFFFFF")
  $("#outputFeedBack").html(errorMessage)

}

function extractUserParameters(searchObject){
    searchObject["location"] = $("#searchValue").val()
    searchObject["noOfBedrooms"] = $("#noOfBedrooms").val()
    if (searchObject["noOfBedrooms"] != "all" && searchObject["noOfBedrooms"] != ">3 BHK")
      searchObject["noOfBedrooms"] = searchObject["noOfBedrooms"].split(" ")[1]
    else if (searchObject["noOfBedrooms"] == ">3 BHK")
      searchObject["noOfBedrooms"] = 4
    return searchObject;
}


function checkForAllParameters(searchObject){
  if(searchObject["location"] == '')
      return false;
  if(searchObject["noOfBedrooms"] == '')
      return false;
  return true;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


$('#searchButton').on('click', function(e) {
  var searchObject = {}
  searchObject = extractUserParameters(searchObject);
  // console.log(searchObject);

  if(searchObject["noOfBedrooms"] ==  "all") {
    finalUrl = 'http://127.0.0.1:5000/api/v1/get-houses-available-for-rent/' + searchObject["location"] + '/' + "all"
  } else {
    finalUrl = 'http://127.0.0.1:5000/api/v1/get-houses-available-for-rent/' +searchObject["location"] + '/' + 
    searchObject["noOfBedrooms"]
  }
  // console.log(finalUrl)
  if(checkForAllParameters(searchObject)){
    
      $.ajax({
        url : finalUrl,
        dataType : "json",
        type : "GET",
        contentType: "application/json",
        xhrFields: {withCredentials: false},
        crossDomain: true,
        data: JSON.stringify(searchObject),
        success: function(data) {
            // Element Creation using DOM needs to be done.
            // console.log(eval(data),typeof(eval(data)));
            var mainDiv=document.querySelector('#searchDisplay');
            var newHeader = document.createElement('h3');
            newHeader.innerHTML = "Search Result";
            mainDiv.appendChild(newHeader);
            for(var i=0;i<data.length;i++){

              var newDiv = document.createElement('div');
              newDiv.setAttribute('class','searchDiv');
                var img = document.createElement('img');
                img.src = "data:image/png;base64," + data[i].image;
                newDiv.appendChild(img);
              // var keys = Object.keys(data[i]);
                console.log(data)
                var hId = document.createElement('p');
                hId.innerHTML = "House ID: "+data[i].houseId;
                var lId = document.createElement('p');
                lId.innerHTML = "Landlord ID: "+data[i].landlordMailId;
                var loc = document.createElement('p');
                loc.innerHTML = "Location: "+data[i].location;
                var nb = document.createElement('p');
                nb.innerHTML = "No of Bedrooms: "+data[i].noOfBedrooms;
                var desc = document.createElement('p');
                desc.innerHTML = "Description: "+data[i].description;
                var rtng = document.createElement('p');
                rtng.innerHTML = "Ratings: "+data[i].rating;
                var br = document.createElement('br');

                var button = document.createElement("button")
                button.innerHTML = "Book Home"
                button.setAttribute("id", data[i].houseId)
                // button.setAttribute("onclick", bookHome)
                button.style.color = "#FFFFFF"
                button.style.background = "#333"


                newDiv.appendChild(hId);
                // newDiv.appendChild(br);
                newDiv.appendChild(lId);
                // newDiv.appendChild(br);
                newDiv.appendChild(loc);
                // newDiv.appendChild(br);
                newDiv.appendChild(nb);
                // newDiv.appendChild(br);
                newDiv.appendChild(desc);
                // newDiv.appendChild(br);
                newDiv.appendChild(rtng);

                newDiv.appendChild(button)
              mainDiv.appendChild(newDiv);

              $("#" + data[i].houseId).on('click', function(e){
                var houseObject = {}
                houseObject["houseId"] = this.id
                console.log(houseObject)
                $.ajax({
                      url : "http://127.0.0.1:5000/api/v1/book-home",
                      dataType : "json",
                      type : "POST",
                      contentType: "application/json",
                      xhrFields: {withCredentials: false},
                      crossDomain: true,
                      data: JSON.stringify(houseObject),
                      success: function(data) {
                        // location.reload();
                        console.log("Data ")
                      },
                      error : function(jqXHR, textStatus, errorThrown){
                          console.log(jqXHR, textStatus, errorThrown);
                      }
              });
              // document.getElementById(data[i].houseId).innerHTML
              // button.addEventListener("onclick", bookHome, false);
              
              // var p = document.createElement('p');
            })}
        },
        error: function(jqXHR, textStatus, errorThrown) {
          statusCode = (jqXHR.status);
          raiseError("No Property Found with the search!");
        }
      });
  } else {
      raiseError("Please Provide Proper Input")
  }           
});


