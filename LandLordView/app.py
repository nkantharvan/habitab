from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify, make_response, Response
import requests
from datetime import datetime
from flask import url_for
import dbdata as db
import os
import base64
from datetime import timedelta
import pickle
import sqlite3
import numpy as np
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
import os
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import pandas as pd

#WARNING !! : Mongo INIT
from pymongo import MongoClient
from gridfs import GridFS
from bson import objectid

client = MongoClient() #localhost:27017
mongodb = client['mygrid']
gfs = GridFS(mongodb)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'xxxxxxxxx'
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=30)


total_number_of_request = 0
def request_counter(function_name):
	global total_number_of_request
	total_number_of_request += 1
	def function(*args):
		return function_name(*args)
	return function


def get_geocoding(location):
    access_token = "498f3c51-5179-49a2-8914-c8ebf05fe7bb"   # update this every 24 hours
    URL = "https://atlas.mapmyindia.com/api/places/geocode?"

    address = location
    auth = access_token

    PARAMS = {'address':address}
    HEADERS = {'Authorization':access_token}

    response = requests.get( url=URL, params= PARAMS, headers = HEADERS)
    if response.status_code == 200:
        data = response.json()
        lat = data['copResults']['latitude']
        lon = data['copResults']['longitude']
        return lat,lon

    else:
        print(response.status_code)


@app.route("/api/v1/get-total-number-of-request", methods = ["GET", "POST", "DELETE", "PUT"])
def get_total_number_of_request():
	if request.method == "GET":
		global total_number_of_request
		return jsonify({"totalNumberOfUserViewRequest" : total_number_of_request}), 200
	else:
		return jsonify({}), 405

#Util functions for Mongo Doc Store
@request_counter
@app.route("/getAllFilesInDb/",methods=['GET'])
def getAllFilesInDb():
    content=""
    files = list(mongodb.fs.files.find())
    for file in files:
        content += str(file["meta"])
    return content


def getFiles(pid):
    content=""
    filter = {"meta.property_id":str(pid)}
    files = list(mongodb.fs.files.find(filter))
    return files


#Landlord View
@request_counter
@app.route('/api/v1/landlord_signup', methods = ["POST", "GET", "DELETE", "PUT"])
def landlord_signup():
    if request.method == "POST":
        landlord_object = request.get_json()

        location_coordinates = get_geocoding(landlord_object["address"] + landlord_object["place"])      
        landlord_object["latitude"] = location_coordinates[0]
        landlord_object["longitude"] = location_coordinates[1]

        parameters = list()
        parameters.extend(["emailId"])

        if db.check_name_in_db(landlord_object, parameters) == False:
	           db.add_user_to_db(landlord_object)
	           return jsonify({}), 201
        else:
	           return jsonify({}), 400
    else:
        return jsonify({}), 405


@request_counter
@app.route('/api/v1/landlord_login', methods = ["POST", "GET", "DELETE", "PUT"])
def landlord_login():
    if request.method == "POST":
        landlord_object = request.get_json()
        parameters = list()
        parameters.extend(["emailid", "pwd"])

        print(db.check_name_in_db(landlord_object, parameters))
        if db.check_name_in_db(landlord_object, parameters):
            session["logged_in"] = True
            session["name"] = landlord_object["emailid"]
            session.permanent = True
            return jsonify({}), 200
        else:
            return jsonify({}), 400

    else:
        return 405


#Property View
@request_counter
@app.route('/api/v1/add_property',methods = ["POST", "GET", "DELETE", "PUT"])
def add_property():
	if request.method == "POST":
		print(request.form)
		property_object = {}

		image_file = request.files['image']
		image_read = image_file.read()
		img_b64 = base64.encodestring(image_read)

		property_object["landlordEmailId"] = session.get("name")
		property_object["latitude"],property_object["longitude"] = get_geocoding(request.form["locality"])
		property_object["locality"] = request.form["locality"]
		property_object["noOfBedrooms"] = request.form["bedrooms"]
		property_object["rentAmount"] = request.form["rentAmount"]
		property_object["description"] = request.form["description"]
		property_object["image"]= img_b64.decode('utf-8')

		print(property_object)

		db.add_property_to_db(property_object)
		return redirect(url_for('landlordview'))
	else:
		return jsonify({}),405


@request_counter
@app.route("/api/v1/add_file/<pid>", methods=['POST'])
def addFiles(pid):
    if request.method == 'POST':
        file = request.files['file']
        if file:
            meta_obj = {"filename": file.filename ,"property_id" : pid ,"uploaded_at": datetime.now() }
            fid = gfs.put(file,meta=meta_obj)
            return redirect("/landlordview/property/"+str(pid))
        else:
            return jsonify({}),405


@request_counter
@app.route("/api/v1/download/<filename>", methods=['GET'])
def downloadFile(filename):
	if request.method == 'GET':
		grid_fs_file = gfs.find_one({"meta.filename":filename})
		response = make_response(grid_fs_file.read())
		response.headers['Content-Type'] = "application/octet-stream"
		response.headers['Content-Disposition'] = "attachment; filename={}".format(filename)
		return response
	else:
		return jsonify({}),405


@request_counter
@app.route("/api/v1/approve-request", methods = ["POST"])
def approve_property():
	if request.method == "POST":
		data = request.get_json()
		house_id = data["houseId"]
		db.approve_tenant_request(house_id)
		return jsonify({}), 200
	else:
		return jsonify({}), 405


#template renders
@request_counter
@app.route("/")
def homepage():
	return render_template("Index.html")


@request_counter
@app.route("/signup_page")
def signup_page():
	return render_template("Signup.html")


@request_counter
@app.route("/login_page")
def login_page():
	if not session.get('logged_in'):
		return render_template("Login.html")
	else:
		return render_template("Index.html")


@request_counter
@app.route("/user_logout")
def user_logout():
	session["logged_in"] = False
	return redirect("/login_page")


@request_counter
@app.route("/landlordview")
def landlordview():
    if session.get("logged_in"):
        properties = db.get_all_properties(session.get("name"))
        print("PropertyAssignment", properties)
        if properties != False:
            images = [ property[2] for property in properties]
            locations = [ property[3] for property in properties]
            locations = list(set(locations))
            return render_template("landlordview.html",len = len(properties), properties = properties, 
                images = images, locations = locations, location_selected = "all", propertiesExist = True)
        else:
            return render_template("landlordview.html", propertiesExist = False)
    else:
        return render_template("Login.html")


@request_counter
@app.route("/landlordview/<location>")
def landlordviewInLocation(location):
    if session.get("logged_in"):
    	properties = db.get_all_properties(session.get("name"))
    	locations = [ property[3] for property in properties]
    	properties = [ property for property in properties if property[3]==location ]
    	images = [ property[2] for property in properties]
    	locations = list(set(locations))
    	return render_template("landlordview.html",len=len(properties),properties=properties,images=images,locations=locations,location_selected=location)
    else:
        return render_template("Login.html")


@request_counter
@app.route("/landlordview/property/<pid>")
def showproperty(pid):
    if session.get("logged_in"):
        propinfo = db.get_property_info(pid)[0]
        tenantinfo = db.get_prop_tenant_info(pid)
        print(tenantinfo)
        if(len(tenantinfo)!= 0):
            tenantinfo = tenantinfo[0]
            files = getFiles(pid)
            return render_template("property.html",propinfo=propinfo , tenantinfo=tenantinfo , files=files)
        else:
            return render_template("Login.html")

#Rent Predictor
filename = 'regression_model.sav'
model = pickle.load(open(filename, 'rb'))

def preprocess(req):
    data=[]
    bhkoption = [0 for i in range(3)]
    typeoption = [0 for i in range(8)]
    lat,lon = get_geocoding(req["locality"])
    data.append(lat)
    data.append(lon)
    data.append(req["area"])
    data.append(req["bedrooms"])
    print(req["BHKorRK"])
    bhkoption[eval(req["BHKorRK"])]=1
    data = data + bhkoption
    typeoption[eval(req["TypeOfHouse"])]=1
    data = data + typeoption

    return data

def analyze(data):
    return model.predict(data.reshape(1,-1))

@request_counter
@app.route("/api/v1/getRent",methods=['POST'])
def showAnalytics():
    print(request.get_json())
    data = preprocess(request.get_json())
    ans = analyze(np.array(data))
    print(ans)
    return jsonify({"Rent":ans[0]}),200

# Analytics View
@app.route("/getAnalytics/<pid>", methods=["GET"])
def getAnalytics(pid):
    if session.get("logged_in"):
        properties = db.get_all_properties(session.get("name"))
        issues = getIssuesForHouse(pid)
        print(issues)
        if(issues=={}):
            return render_template("noAnalytics.html")
        createSeasonalChart(issues)
        createLineChart(issues)
        createWordCloud(issues)
        points,center,length,counts = get_company_landmarks(pid)
        return render_template("analytics.html",points = points,centre=center,len=len(points),counts=counts,properties=properties,pid_selected=pid)
    else:
        return render_template("Login.html")

def get_company_landmarks(pid):
    lat = 12.894033
    lon = 77.6623617
    df = pd.read_csv('Files/tenant_company.csv')
    access_token = "bearer 498f3c51-5179-49a2-8914-c8ebf05fe7bb"
    URL = "https://atlas.mapmyindia.com/api/places/nearby/json?"

    keyword = "software company"

    reflocation = ""+str(lat)+","+str(lon)
    HEADERS = {'Authorization':access_token}

    PARAMS = {'keywords':keyword,'refLocation':reflocation}

    r = requests.get( url = URL, params = PARAMS, headers=HEADERS)
    print("STATUS",r.status_code)


    if r.status_code == 200:
        data = r.json()
        points = []
        counts=[[] for i in range(0,5)]
        for i in range(5):
            if(data['suggestedLocations'][i]['distance']<4000):
                points.append([])
                points[i].append(data['suggestedLocations'][i]['entryLatitude'])
                points[i].append(data['suggestedLocations'][i]['entryLongitude'])
                counts[i].append(data['suggestedLocations'][i]['placeName'])
                counts[i].append(0)
                for j in range(len(df)):
                    if(df.loc[j,'company'] == data['suggestedLocations'][i]['placeName']):
                        counts[i][1]+=1
    return points,[lat,lon],len(points),counts

# This logic is flawed as it identifies a property based on the current tenant living there.
# For the dummy data added this is a smart fix
def getIssuesForHouse(pid):
    connection_state = sqlite3.connect("habitab_analytics.db")
    cursor = connection_state.cursor()
    cursor.execute('''SELECT tenantEmailid from PropertyAssignment where pid = ?''',[pid])
    tenantEmail = cursor.fetchone()
    if(tenantEmail==None):
        return {}
    else:
        tenantEmail = tenantEmail[0]

    cursor.execute('''SELECT IssueDate,category,description from Issue where reporterEmailid = ?''',[tenantEmail])
    issues = cursor.fetchall()
    connection_state.commit()
    connection_state.close()

    return issues

def createWordCloud(issues):
    text = " ".join(issue[2] for issue in issues)
    if(text==""):
        return
    stopwords = set(STOPWORDS)
    wordcloud = WordCloud(stopwords=stopwords,max_font_size=50, max_words=100, background_color="white").generate(text)
    plt.figure()
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.savefig("static/wordcloud.png")
    plt.close()

def createLineChart(issues):
    x = range(1,13)
    y_elec=[0 for i in range(12)]
    y_plumb=[0 for i in range(12)]
    y_pest=[0 for i in range(12)]
    y_carp=[0 for i in range(12)]

    for issue in issues:
        month = datetime.strptime(issue[0], "%Y-%m-%d").month
        cat = issue[1]
        if(cat == "Electrician"): y_elec[month-1]+=1
        elif(cat == "Plumber"): y_plumb[month-1]+=1
        elif(cat == "Pest Control"): y_pest[month-1]+=1
        elif(cat == "Carpenter"): y_carp[month-1]+=1

    plt.style.use('seaborn-darkgrid')
    palette = plt.get_cmap('Set1')
    plt.plot(x, y_elec, marker='', color=palette(1), linewidth=1, alpha=0.9, label="Electrician")
    plt.plot(x, y_plumb, marker='', color=palette(2), linewidth=1, alpha=0.9, label="Plumber")
    plt.plot(x, y_pest, marker='', color=palette(3), linewidth=1, alpha=0.9, label="Pest Control")
    plt.plot(x, y_carp, marker='', color=palette(4), linewidth=1, alpha=0.9, label="Carpenter")

    plt.legend(loc=2, ncol=2)
    plt.xlabel("Months")
    plt.ylabel("Frequency of Issue")
    plt.savefig("static/linechart.png")
    plt.close()



def createSeasonalChart(issues):
    electrianIssues=[]
    plumberIssues=[]
    pestControlIssues=[]
    carpenterIssues=[]

    summer = [3,4,5,6]
    monsoon = [7,8,9,10]
    winter = [11,12,1,2]

    for issue in issues:
        if("Electrician" in issue): electrianIssues.append(issue)
        elif("Plumber" in issue): plumberIssues.append(issue)
        elif("Pest Control" in issue): pestControlIssues.append(issue)
        elif("Carpenter" in issue): carpenterIssues.append(issue)

    #bars for season and values for every category
    bar1 = [0,0,0,0]
    bar2 = [0,0,0,0]
    bar3 = [0,0,0,0]

    for issue in electrianIssues:
        month = datetime.strptime(issue[0], "%Y-%m-%d").month
        if month in summer: bar1[0]+=1
        elif month in monsoon: bar2[0]+=1
        elif month in winter: bar3[0]+=1

    for issue in plumberIssues:
        month = datetime.strptime(issue[0], "%Y-%m-%d").month
        if month in summer: bar1[1]+=1
        elif month in monsoon: bar2[1]+=1
        elif month in winter: bar3[1]+=1

    for issue in pestControlIssues:
        month = datetime.strptime(issue[0], "%Y-%m-%d").month
        if month in summer: bar1[2]+=1
        elif month in monsoon: bar2[2]+=1
        elif month in winter: bar3[2]+=1

    for issue in carpenterIssues:
        month = datetime.strptime(issue[0], "%Y-%m-%d").month
        if month in summer: bar1[3]+=1
        elif month in monsoon: bar2[3]+=1
        elif month in winter: bar3[3]+=1

    bar1 = np.array(bar1)
    bar2 = np.array(bar2)
    bar3 = np.array(bar3)
    r = [1,2,3,4]
    names = ['Electrician','Plumber','Pest Control','Carpenter']
    plt.bar(r, bar3, bottom = bar1+bar2,color='#665191', edgecolor='white', width=1 )
    plt.bar(r, bar2, bottom=bar1, color='#003f5c', edgecolor='white', width=1)
    plt.bar(r, bar1,  color='#ff6361',edgecolor='white', width=1)
    plt.legend(["winter","monsoon","summer"],loc=1)
    plt.xticks(r, names, fontweight='bold')
    plt.xlabel("Issue Category")
    plt.savefig("static/barchart.png")
    plt.close()


@app.route("/new_request")
def new_request():
		return Response(db.push_data(session.get("name")), mimetype="text/event-stream")
        # return Response(db.push_data("mmshetty.98@gmail.com"), mimetype="text/event-stream")


@request_counter
@app.route("/view_request")
def view_request():
    # new_house_request_data = db.get_new_house_request_data("mmshetty.98@gmail.com")
    # print(new_house_request_data)
    if session.get("logged_in"):
        new_house_request_data = db.get_new_house_request_data(session.get("name"))
        return render_template("NewRequest.html", new_house_request_data = new_house_request_data)
    else:
        render_template("Login.html")


if __name__ == '__main__':
	app.secret_key = os.urandom(12)
	app.run(debug=True,port=5004)
