import sqlite3
from datetime import date

def add_user_to_db(landlord_data):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	cursor.execute('''
					INSERT INTO Landlord 
						(emailid,name ,pwd ,phno,address,place ,state ,latitude,longitude) 
						VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)'''
					,[landlord_data["emailId"],landlord_data["userName"],landlord_data["password"],landlord_data["phoneNumber"]
					,landlord_data["address"],landlord_data["place"],landlord_data["state"],landlord_data["latitude"],landlord_data["longitude"]])
	connection_state.commit()
	connection_state.close()


def check_name_in_db(user_data, key):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	query_parameter = [user_data[parameter] for parameter in key]
	parameter_length = len(key)
	query_clause = str()
	
	for i in range(parameter_length):
		if i != parameter_length - 1:
			query_clause += key[i] + " = ? and "
		else:
			query_clause += key[i] + " = ? "
	
	query = "SELECT * from Landlord  where " + query_clause
	cursor.execute(query, query_parameter)
	user_data = cursor.fetchone()
	connection_state.commit()
	connection_state.close()
	
	if user_data == None:
		return False

	if len(user_data) == 0:
		return False

	if user_data != None:
		return True
	if len(user_data) != 0:
		return True

def add_property_to_db(prop_data):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = '''INSERT into Property
				(description,image,locality,latitude,longitude,noOfBedrooms,rentAmount,landlordEmailId, rating, numberOfPeopleRated)
				VALUES(?,?,?,?,?,?,?,?,?, ?)'''
	cursor.execute(query,
					[prop_data["description"],
					prop_data["image"],
					prop_data["locality"],
					prop_data["latitude"],
					prop_data["longitude"],
					prop_data["noOfBedrooms"],
					prop_data["rentAmount"],
					prop_data["landlordEmailId"],
					0,
					0])
					
	connection_state.commit()
	
	cursor.execute("SELECT last_insert_rowid()")
	pid = cursor.fetchone()[0]

	query = '''	INSERT INTO PropertyAvalaibilityStatus (pid, tenantApproval, landlordApproval) VALUES (?, ?, ?)'''
	cursor.execute(query, [pid, 0, 0])

	connection_state.commit()
	connection_state.close()

def get_all_properties(name):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	cursor.execute("SELECT * from Property where landlordEmailId = ?",
					[name])
	data = cursor.fetchall()

	print("In get property function", data)
	if len(data) == 0:
		return False
	
	connection_state.commit()
	connection_state.close()
	return data

def get_property_info(pid):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	cursor.execute("SELECT * from Property where pid= ?",
					[pid])
	data = cursor.fetchall()
	connection_state.commit()
	connection_state.close()
	return data

def get_prop_tenant_info(pid):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()
	cursor.execute(''' SELECT * from User
						where emailid in ( SELECT tenantEmailid from PropertyAssignment
											where pid = ?) ''',
						[pid])
	data = cursor.fetchall()
	connection_state.commit()
	connection_state.close()
	return data


def approve_tenant_request(house_id):
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = "UPDATE PropertyAvalaibilityStatus SET landlordApproval = 1 where pid = ?"
	cursor.execute(query, [house_id])
	connection_state.commit()

	query = '''SELECT * FROM TenantInterest where pid = ?'''
	cursor.execute(query, [house_id])
	tenant_email_id = cursor.fetchone()[1]
	connection_state.commit()

	query = '''DELETE FROM TenantInterest where pid = ?'''
	cursor.execute(query, [house_id])
	connection_state.commit()

	today_date = date.today()
	today_date = today_date.strftime("%d-%m-%y")
	
	query = '''INSERT INTO PropertyAssignment (pid, tenantEmailid, rentReminderDate) VALUES (?, ?, ?)'''
	connection_state.execute(query, [house_id, tenant_email_id, today_date])
	connection_state.commit()

	connection_state.close()


def get_new_house_request_data(landlord_emailId):

	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = ''' SELECT Property.pid, Property.noOfBedrooms, Property.locality, Property.latitude, Property.longitude, Property.description,
				Property.image, Property.landlordEmailId, Property.rentAmount, User.userName, User.emailId,  User.address, User.place
				FROM Property INNER JOIN TenantInterest ON Property.pid = TenantInterest.pid and Property.landlordEmailId = ?
				INNER JOIN User ON TenantInterest.tenantEmailId = User.emailId
			'''
	cursor.execute(query, [landlord_emailId])

	house_data = cursor.fetchall()
	connection_state.commit()
	connection_state.close()
	
	house_board = list()
	for data in house_data:
		details = dict()
		details["houseId"] = data[0]
		details["noOfBedrooms"] = data[1]
		details["locality"] = data[2]
		details["latitude"] = data[3]
		details["longitude"] = data[4]
		details["description"] = data[5]
		details["image"] = data[6]
		details["landlordEmailId"] = data[7]
		details["rentAmount"] = data[8]
		details["tenantName"] = data[9]
		details["tenantEmailId"] = data[10]
		details["tenantCurrentPlace"] = data[11]
		house_board.append(details)

	return house_board

def push_data(landlord_emailId):
	
	print(landlord_emailId)
	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()	

	query = ''' SELECT COUNT(*) FROM Property INNER JOIN TenantInterest ON Property.pid = TenantInterest.pid and Property.landlordEmailId = ?
			'''
	cursor.execute(query, [landlord_emailId])
	number_of_request = cursor.fetchone()
	connection_state.close()

	print(number_of_request)

	if number_of_request != None:

		yield 'data: %s\n\n' % 'New Request:{0}'.format(number_of_request[0])
	else:
		yield 'data: %s\n\n' % 'New Request:{0}'.format(0)
