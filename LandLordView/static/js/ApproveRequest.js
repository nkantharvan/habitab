var serverAddress = '127.0.0.1'
var portNumber = '5004'

function raiseError(errorMessage) {
    $("#outputFeedBack").css("display","block")
    $("#outputFeedBack").css("color","#FFFFFF")
    $("#outputFeedBack").html(errorMessage)
  }


function checkForAllParameters(requestObject){
    if(requestObject["houseId"] == '')
        return false
    return true;
  }

  
function approveRequest(houseId){
  var requestObject = {}
  requestObject["houseId"] = houseId
  console.log(requestObject)
  if(checkForAllParameters(requestObject)){
      $.ajax({
        url : 'http://' + serverAddress + ':' + portNumber + '/api/v1/approve-request',
        dataType : "json",
        type : "POST",
        contentType: "application/json",
        xhrFields: {withCredentials: false},
        crossDomain: true,
        data: JSON.stringify(requestObject),
        success: function(data) {
            window.location.href = "http://" + serverAddress + ":" + portNumber + "/landlordview"
        },
        error: function(jqXHR, textStatus, errorThrown) {
          statusCode = (jqXHR.status);
          raiseError("Please Provide Proper Credentials")
        }
      });
  } else {
      raiseError("Please Provide Proper Credentials")
  }           
}
  