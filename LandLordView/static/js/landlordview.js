var serverAddress = '127.0.0.1'
var portNumber = '5004'

function raiseError(errorMessage) {
    $("#outputFeedBack").css("display","block")
    $("#outputFeedBack").css("color","#FFFFFF")
    $("#outputFeedBack").html(errorMessage)
  }

function extractPropertyParameters(propertyObject){
    propertyObject["locality"] = $("#locality").val()
    propertyObject["noOfBedrooms"] = $("#noOfBedrooms").val()
    propertyObject["rentAmount"] = $("#rentAmount").val()
    propertyObject["description"] = $("#description").val()
    // propertyObject["image"] = $("#form_right").find("#image")[0].files[0];
    return propertyObject;
}

function checkForAllParameters(propertyObject){
    if(propertyObject["locality"]=='')
        return false
    if(propertyObject["noOfBedrooms"]=='')
        return false
    if(propertyObject["rentAmount"]=='')
        return false
    if(propertyObject["description"]=='')
        return false
    // if(propertyObject["image"]=='')
        // return false
        
    return true;
  }

  

  