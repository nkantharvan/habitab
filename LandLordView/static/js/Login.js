var serverAddress = '127.0.0.1'
var portNumber = '5004'

function raiseError(errorMessage) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#FFFFFF")
  $("#outputFeedBack").html(errorMessage)
}

function extractUserParameters(userObject){
    userObject["emailid"] = $("#emailid").val()
    userObject["pwd"] = $("#pwd").val()
    return userObject;
}

function checkForAllParameters(userObject){
  if(userObject["emailid"] == '')
      return false;
  if(userObject['pwd'] == '')
      return false;
  return true;
}

$('#user_login').on('click', function(e) {

  var userObject = {}
  userObject = extractUserParameters(userObject);
  if(checkForAllParameters(userObject)){
      $.ajax({
        url : 'http://' + serverAddress + ':' + portNumber + '/api/v1/landlord_login',
        dataType : "json",
        type : "POST",
        contentType: "application/json",
        xhrFields: {withCredentials: false},
        crossDomain: true,
        data: JSON.stringify(userObject),
        success: function(data) {
            window.location.href = 'http://' + serverAddress + ":" + portNumber + '/landlordview'
        },
        error: function(jqXHR, textStatus, errorThrown) {
          statusCode = (jqXHR.status);
          raiseError("The given email id is already registered with us")
        }
      });
  } else {
      raiseError("Please Provide Proper Credentials")
  }
});
