var serverAddress = '127.0.0.1'
var portNumber = '5002'

function raiseError(errorMessage) {
  $("#outputFeedBack").css("display","block")
  $("#outputFeedBack").css("color","#FFFFFF")
  $("#outputFeedBack").html(errorMessage)
}

function extractUserParameters(serviceManObject){
    serviceManObject["emailId"] = $("#emailId").val()
    serviceManObject["password"] = $("#password").val()
    return serviceManObject;
}

function checkForAllParameters(serviceManObject){
  if(serviceManObject["emailId"] == '')
      return false;
  if(serviceManObject['password'] == '')
      return false;
  return true;
}

$('#user_login').on('click', function(e) {
          
  var serviceManObject = {}
  serviceManObject = extractUserParameters(serviceManObject);
  if(checkForAllParameters(serviceManObject)){
  else {
      raiseError("Please Provide Proper Credentials")
  }           
}