import sqlite3

def get_statistics_from_db():

	connection_state = sqlite3.connect("/home/manish/manish/Manish/Documents/7th_Sem/SE/habitab/Habitab.db")
	cursor = connection_state.cursor()

	query = "SELECT count(*) from ServiceMan"
	cursor.execute(query)
	total_serviceman = cursor.fetchone()[0]

	query = "SELECT count(*) from User"
	cursor.execute(query)
	total_user = cursor.fetchone()[0]

	query = "SELECT count(*) from Issue"
	cursor.execute(query)
	total_number_issue_reported = cursor.fetchone()[0]

	query = "SELECT count(*) from Landlord"
	cursor.execute(query)
	total_landlord = cursor.fetchone()[0]

	query = "SELECT count(*) from Property"
	cursor.execute(query)
	total_number_properties = cursor.fetchone()[0]

	platform_data = dict()
	platform_data["Total Number Of SerivceMan"] = total_serviceman
	platform_data["Total Number Of User"] = total_user
	platform_data["Total Number Of Landlord"] = total_landlord
	platform_data["Total Number Of Issue Reported"] = total_number_issue_reported
	platform_data["Total Number Of Properties"] = total_number_issue_reported

	return platform_data