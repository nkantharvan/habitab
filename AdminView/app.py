from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify, Response
import requests
from datetime import date
from datetime import timedelta
import flask_login
import justdialapi
import dbdata as db
import os

app = Flask(__name__)
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=30)

login_manager = flask_login.LoginManager()

login_manager.init_app(app)


@app.route('/api/v1/admin_login', methods = ["POST", "GET", "DELETE", "PUT"])
def user_login():
	if request.method == "POST":
		user_object = request.get_json()
		parameters = list()
		parameters.extend(["emailId", "password"])
		print(user_object)
		if user_object["emailId"] == "msoni6226@gmail.com" and user_object["password"] == "admin":
			session["logged_in"] = True
			session["name"] = user_object["emailId"]
			session.permanent = True
			return jsonify({}), 200
		else:
			return jsonify({}), 404
	
	else:
		return jsonify({}), 405

@app.route("/get_statistics")
def get_statistics():
	# if not session.get('logged_in'):
		global total_number_of_request
		
		# user_view_response = requests.get("http://127.0.0.1:5000/api/v1/get-total-number-of-request")
		# user_view_response = user_view_response.json()
		# user_view_request_count = user_view_response["totalNumberOfUserViewRequest"]

		# service_man_view_response = requests.get("http://127.0.0.1:5001/api/v1/get-total-number-of-request")
		# service_man_view_response = service_man_view_response.json()
		# service_man_view_request_count = service_man_view_response["totalNumberOfServiceManViewRequest"]

		# landlord_view_response = requests.get("http://127.0.0.1:5004/api/v1/get-total-number-of-request")
		# landlord_view_response = landlord_view_response.json()
		# landlord_view_response_count = landlord_view_response["totalNumberOfUserViewRequest"]

		platform_data = db.get_statistics_from_db()
		# platform_data["Total Number Of Request"] = (service_man_view_request_count + user_view_request_count + landlord_view_response_count)

		return render_template("Statistics.html", platform_data = platform_data)
		# else:
		# 	return render_template("Login.html")


@app.route("/get_service_man_details")
def get_service_man_details():
	return render_template("ServiceMan.html")

@app.route("/view_service_man_details", methods = ["POST"])
def view_service_man_details():
	print(request.form)
	serivce_man_type = request.form["category"]
	location = request.form["location"]
	print(serivce_man_type, location)
	service_man = justdialapi.get_serviceman_name(serivce_man_type, location)
	print(service_man)
	return render_template("ServiceManDetails.html", service_man_details = service_man)
	# return "Hello World"

@app.route("/login_page")
def login_page():
	if not session.get('logged_in'):
		return render_template("Login.html")
	else:
		return render_template("Index.html")

@app.route("/signup_page")
def signup_page():
	return render_template("Signup.html")


@app.route("/user_logout")
def user_logout():
	session["logged_in"] = False
	return render_template("Login.html")


@app.route("/homepage")
def homepage():
	return render_template("Index.html")

if __name__ == "__main__":
	app.secret_key = os.urandom(12)
	app.run(debug=True, port = 5002)